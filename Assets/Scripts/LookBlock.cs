﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookBlock : Block
{
    // Start is called before the first frame update
    public enum Subtype {Say,Think,SwitchImage,SwitchBg,ScaleSize,ChangeColor,Clear,Show,Hide,ChangeLayer};

    private BlockType.Catagory catagory = BlockType.Catagory.Looks;
    public Subtype blockSubtype;
    public override void Start()
    {
        base.Start();
       ReadImage(catagory.ToString(),blockSubtype.ToString());
    }

}
