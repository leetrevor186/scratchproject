﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OperatorBlock : Block
{
    // Start is called before the first frame update
    public enum Subtype {Sum,Reduce,Multiply,Divide,Random,LargerThan,SmallerThan,And,Or,Not,Join,CharPosition,ContainsString,StringLength,Mod,Round,AngleOf};


    private BlockType.Catagory catagory = BlockType.Catagory.Operators;
    public Subtype blockSubtype;
    public override void Start()
    {
        base.Start();
       ReadImage(catagory.ToString(),blockSubtype.ToString());
    }


}
