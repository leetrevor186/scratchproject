﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ShadowBlock : MonoBehaviour
{
    public Image shadowImage;
    public float height{
        get;
        set;
    }
    public float width{
        get;
        set;
    }

    public void SetBlkProperties(float h,float w,Sprite imageSprite){
        shadowImage.sprite = imageSprite;
        height = h;
        width =w;
        gameObject.GetComponent<RectTransform>().sizeDelta = imageSprite.rect.size;

    }
}
