﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class Block : MonoBehaviour, IDragHandler, IEndDragHandler
{   
    const int BOT_SPACE = 30;
    const int TOP_SPACE = 30;
    const float HOLE_HEIGHT = 250;

    const float TopPuzzlePart = 252;
    const float BotPuzzlePart = 223;

    public BlockType blockType;
    public float height;
    public float totalHeight;
    public int holeNumber;
    public float width;

    public float holeHeight;
    public bool collisionBool;
    public Collider2D  collideItem;
    public List<Vector2> startingPointList = new List<Vector2>();
    private List<Vector2> InnerstartingPointList = new List<Vector2>();

    public List<Block> blkList = new List<Block>();
    public List<Block> backupBlkList = new List<Block>();
    public List<Block> innerList = new List<Block>();
    public List<Block> backupInnerList = new List<Block>();

    public Dictionary<string,Vector2> boxColliderPosList = new Dictionary<string,Vector2>();
    public bool childOfBlock = false; 
    public List<Collider2D> colliderList = new List<Collider2D>();
    public CanvasManager canvasManager;
    public Block collidingBlock;
    public Block parentBlock;
    public Block innerBlock;
    public Collider2D parentCollider;
    private bool dragBool;
    private Vector3 touchOffset;
    private bool startDrag;
    private Block currentDragBlock;
    public bool previewBool;
    public GameObject previewShadow;
    public BlockType blkType;
    public Image blockImage;
    public Dictionary<string,bool> colliderSwitchList= new Dictionary<string,bool>();
    public bool innerHoleBool;
    private bool _initCollider = false;

    public virtual void Start(){
        height = this.GetComponent<Image>().sprite.rect.height;
        totalHeight = height;
        width = this.GetComponent<Image>().sprite.rect.width;
        this.GetComponent<RectTransform>().sizeDelta = new Vector2(width,height);
        
        holeHeight = HOLE_HEIGHT;
        blkList.Add(this);

        startingPointList.Add(new Vector2(60,height/2));    // Left Top Corner , No hole
        startingPointList.Add(new Vector2(0,height/2));      // Left Top Corner , 1 hole
        InnerstartingPointList.Add(new Vector2(120,0));
        InnerstartingPointList.Add(new Vector2(180,0));

        InitCollider();
        foreach(BoxCollider2D obj in colliderList)
            colliderSwitchList.Add(obj.tag,true);

        innerHoleBool = holeNumber == 0 ? false : true;
    }

    void Update(){
        foreach(BoxCollider2D obj in colliderList)
            obj.enabled = colliderSwitchList[obj.tag];
        if(_initCollider)
        {   
            if(holeNumber ==1)
            colliderList[3].GetComponent<BoxCollider2D>().size =new Vector2 (100,this.GetComponent<RectTransform>().sizeDelta.y);
        }
    }
    public void InitCollider(){
        foreach(BoxCollider2D obj in colliderList)
            boxColliderPosList.Add(obj.tag,obj.offset);
            _initCollider= true;

    }

    
    public void InsertBlock(string colliderPosition,GameObject obj,bool shadow = false){

        bool signedBool = this.holeNumber == collidingBlock.holeNumber;
        int signed = holeNumber - collidingBlock.holeNumber;
        

        switch(colliderPosition){
            case "TopCollider": 
            if(shadow){
                obj.transform.SetParent(collideItem.transform);
                obj.transform.localPosition = new Vector2 (signedBool? startingPointList[1].x: signed*startingPointList[0].x, collidingBlock.startingPointList[holeNumber].y + height/2 -TOP_SPACE);
                // obj.transform.localPosition = TopPosList[holeNumber];
            }
                break;
            case "MidCollider": 
                obj.transform.SetParent(collideItem.transform);
                obj.transform.localPosition = new Vector2 (signedBool? InnerstartingPointList[holeNumber].x: InnerstartingPointList[holeNumber].x, 0 );
                if(colliderPosition =="MidCollider" && collidingBlock.blkList.Count>1){
                    bool insertSamePointBool = this.holeNumber == collidingBlock.blkList[0].holeNumber;
                    // collidingBlock.CalculateStartingPoint();
                    float tempY = collidingBlock.totalHeight + Mathf.Max(obj.GetComponent<RectTransform>().sizeDelta.y-holeHeight,0)/2 ;
                    Debug.Log("Show numbers " + tempY + " 1 " + collidingBlock.GetComponent<RectTransform>().sizeDelta.y +" 2 " + obj.GetComponent<RectTransform>().sizeDelta.y);
                    collidingBlock.blkList[1].transform.localPosition = new  Vector2 (insertSamePointBool? collidingBlock.blkList[1].transform.localPosition.x: signed*startingPointList[1].x , -collidingBlock.GetComponent<RectTransform>().sizeDelta.y/2);
                }
                break; 

            case "BotCollider": 
                obj.transform.SetParent(collideItem.transform);

                obj.transform.localPosition = new Vector2 (signedBool? startingPointList[1].x: signed*startingPointList[0].x, collidingBlock.startingPointList[holeNumber].y - collidingBlock.GetComponent<RectTransform>().sizeDelta.y - height/2 +TOP_SPACE );
                
                if(collidingBlock.blkList.Count>1){
                    bool insertSamePointBool = this.holeNumber == collidingBlock.blkList[1].holeNumber;
                    collidingBlock.blkList[1].transform.localPosition = new  Vector2 (insertSamePointBool? obj.transform.localPosition.x: signed*startingPointList[1].x , collidingBlock.startingPointList[holeNumber].y - collidingBlock.GetComponent<RectTransform>().sizeDelta.y - collidingBlock.blkList[1].height/2 - obj.GetComponent<RectTransform>().sizeDelta.y +TOP_SPACE*2);
                }
               break;
            case "OutCollider":
                obj.transform.SetParent(collideItem.transform);
                obj.transform.localPosition = new Vector2(-InnerstartingPointList[holeNumber].x,0 );
                obj.GetComponent<RectTransform>().sizeDelta = new Vector2(collidingBlock.width , collidingBlock.totalHeight + obj.GetComponent<RectTransform>().sizeDelta.y - holeHeight);

                Debug.Log("tesaseltn" + this.name);

                // obj.transform.GetComponent<RectTransform>().sizeDelta = new Vector2 (collidingBlock.GetComponent<RectTransform>().sizeDelta.x,collidingBlock.totalHeight);
                break;
            default: break; 
        }
        if(!shadow){
            // otherBlock.height = otherBlock.height+ this.height- holeHeight;
            
            obj.GetComponent<Collider2D>().enabled = false;
            switch (colliderPosition){
            case "TopCollider": 
                    Block blk = obj.GetComponent<Block>();
                    blk.AddToList(blk.backupBlkList,blk.blkList);
                    if(collidingBlock.blkList.Count >1)
                        blk.AddToList(blk.blkList,collidingBlock.blkList);
                    else
                        blk.AddToList(collidingBlock,blk.blkList);

                    Debug.Log(blk.colliderList[2].name);
                    collidingBlock.transform.SetParent(blk.colliderList[blk.holeNumber == 1? 2:1].transform);

                    collidingBlock.parentBlock = this;
                    collidingBlock.parentCollider = this.parentCollider;

                    collidingBlock.colliderSwitchList["TopCollider"] = false;
                    if(this.colliderSwitchList.ContainsKey("MidCollider")) this.colliderSwitchList["MidCollider"] = true ;

                    collidingBlock.colliderSwitchList["BotCollider"] = true;
                    collidingBlock.colliderSwitchList["OutCollider"] = true;
                    this.colliderSwitchList["TopCollider"] = true;
                    if(this.colliderSwitchList.ContainsKey("MidCollider")) this.colliderSwitchList["MidCollider"] = true ;
                    this.colliderSwitchList["BotCollider"] = true;
                    this.colliderSwitchList["OutCollider"] = true;

                    parentCollider = null;

                    collidingBlock.transform.localPosition = new Vector2 (signedBool? startingPointList[1].x: -signed*startingPointList[0].x, startingPointList[holeNumber].y - totalHeight - collidingBlock.height/2 + TOP_SPACE);
                break;
            case "MidCollider": 
                    collidingBlock.colliderSwitchList["MidCollider"] = false;
                    this.colliderSwitchList["TopCollider"] = true;
                    if(this.colliderSwitchList.ContainsKey("MidCollider")) this.colliderSwitchList["MidCollider"] = true ;
                    this.colliderSwitchList["BotCollider"] = true;
                    this.colliderSwitchList["OutCollider"] = true;

                    innerHoleBool = false;
                    collidingBlock.innerBlock = this;
                    this.parentBlock = collidingBlock;

                    float yPos = 0;
                    if(this.blkList.Count>1){
                        for(int i=1; i< blkList.Count ; i++){
                            yPos += blkList[i].height/2;
                        }
                    }
                    obj.transform.localPosition = new Vector2 (signedBool? InnerstartingPointList[holeNumber].x: InnerstartingPointList[holeNumber].x,yPos);

                break; 
            case "BotCollider": 
                    collidingBlock.AddToList(collidingBlock.backupBlkList,collidingBlock.blkList);
                    parentBlock = collidingBlock;

                    Block tempBlk = this.parentBlock;

                    if(collidingBlock.blkList.Count<2){
                        do{
                            if(this.blkList.Count>1)
                                AddToList(tempBlk.blkList,this.blkList);
                            else
                                AddToList(this,tempBlk.blkList);
                            tempBlk = tempBlk.parentBlock;
                        }while(tempBlk !=null);
                    }
                    else
                    {   
                        this.transform.parent.GetChild(0).SetParent(this.transform.Find("BotCollider"));
                        InsertList(this,collidingBlock.blkList,1);
                        collidingBlock.blkList[2].parentBlock = this;
                    }
                    
                    this.colliderSwitchList["TopCollider"] = false;
                    if(this.colliderSwitchList.ContainsKey("MidCollider")) this.colliderSwitchList["MidCollider"] = true ;
                    this.colliderSwitchList["BotCollider"] = true;
                    this.colliderSwitchList["OutCollider"] = true;
                    // collidingBlock.CalculateHeight(collidingBlock.blkList);

                break;
            case "OutCollider":
                this.transform.SetParent(canvasManager.transform);
                collidingBlock.transform.SetParent(this.colliderList[1].transform);

                collidingBlock.parentBlock = this;
                collidingBlock.parentCollider = this.colliderList[1];
                this.innerBlock = collidingBlock;

                this.colliderSwitchList["TopCollider"] = true;
                if(this.colliderSwitchList.ContainsKey("MidCollider")) this.colliderSwitchList["MidCollider"] = false ;
                this.colliderSwitchList["BotCollider"] = true;
                this.colliderSwitchList["OutCollider"] = true;
                
                break;
            default: break;

            }

            AdjustHeight(collidingBlock.blkList, collidingBlock.gameObject,colliderPosition);

        }

        if(previewShadow != null){
            GameObject.Destroy(previewShadow);
        }
    }
    public void InsertList(Block blk,List<Block> targetList,int pos){
        targetList.Insert(pos,blk);
    }
    public void AddToList(Block blk,List<Block> targetList){
        if(!targetList.Contains(blk))
            targetList.Add(blk);
        if(parentBlock != null ){
            parentBlock.AddToList(blk,targetList);
        }
    }
    public void AddToList(List<Block> targetList,List<Block> bList){
        
        foreach(Block blk in bList){
            if(!targetList.Contains(blk)){
                targetList.Add(blk);
            }
        }
        if(parentBlock !=null)
            parentBlock.AddToList(parentBlock.backupBlkList,parentBlock.blkList);

    }
    public void RemoveList(List<Block> targetList,List<Block> bList){
        targetList.Clear();
        targetList.AddRange(bList);
    }
    public void RemoveBlock(List<Block> targetList, Block targetBlock){
        targetList.Remove(targetBlock);
        if(parentBlock != null){
            parentBlock.RemoveBlock(parentBlock.blkList,targetBlock);
        }
        if(innerBlock == targetBlock){
            innerBlock = null;
        }
        backupBlkList.Clear();
        backupBlkList.AddRange(targetList);
    }
    public void PreviewBlock(string colliderPosition){
        if(colliderPosition == "OutCollider" && this.holeNumber ==0) return;
        // if(colliderPosition == "MidCollider" && !collidingBlock.innerHoleBool) return;
        
        GameObject shadow = Object.Instantiate(canvasManager.shadowBlock);
        ShadowBlock shadowBlk = shadow.GetComponent<ShadowBlock>();
        shadowBlk.SetBlkProperties(height,width,blockImage.sprite);
        shadow.transform.SetParent(canvasManager.transform);
        shadow.transform.SetAsFirstSibling();
        if(colliderPosition == "MidCollider")
            collidingBlock.GetComponent<RectTransform>().sizeDelta = new Vector2 (collidingBlock.width, collidingBlock.height +shadowBlk.height-holeHeight);
        // if(colliderPosition == "OutCollider"){
        // }
        InsertBlock(colliderPosition,shadow,true);
        previewShadow = shadow;
    }
    public float GetHeight(){
        float h = 0;
        if( holeNumber==0){
            h = this.GetComponent<RectTransform>().sizeDelta.y - BOT_SPACE;
        }else
        {
            h = this.height;
            if(innerBlock != null)
            {   
                foreach(Block blk in innerBlock.blkList){
                    h += blk.GetHeight();
                }
                h -= this.holeHeight;
            }
        }

        return h;
    }
    public void AdjustHeight(List<Block> blockList,GameObject targetBlock,string colliderPosition = null){
        Block tgBlk = targetBlock.GetComponent<Block>();
        // Debug.Log("Get height" + tgBlk.GetHeight());

        tgBlk.totalHeight = tgBlk.GetHeight();
        Debug.Log("Total Height = " + tgBlk.totalHeight);
        switch(colliderPosition){
            
        case "MidCollider":
            if(tgBlk.innerBlock !=null){
                tgBlk.transform.GetComponent<RectTransform>().sizeDelta = new Vector2 (tgBlk.width,tgBlk.GetComponent<Block>().totalHeight);
                tgBlk.totalHeight = tgBlk.GetComponent<RectTransform>().sizeDelta.y;

            }
            else{
                tgBlk.transform.GetComponent<RectTransform>().sizeDelta = new Vector2 (tgBlk.width,tgBlk.GetComponent<Block>().height);
                tgBlk.totalHeight = tgBlk.GetComponent<Block>().totalHeight;
            }
            tgBlk.CalculateStartingPoint();
            AdjustCollider(colliderPosition,targetBlock);
            break;
        default:
            break;
        }
        if(tgBlk.parentBlock != null){
            tgBlk.AdjustHeight(tgBlk.parentBlock.blkList,tgBlk.parentBlock.gameObject,colliderPosition);
        }
        if(tgBlk.blkList.Count>1){
            // foreach(Block blk in tgBlk.blkList){
            //     blk.AdjustHeight(tgBlk.blkList,blk.gameObject,colliderPosition);
            // }
            Debug.Log(tgBlk.blkList.Count + " SHOW COUNT");
        }
        // tgBlk.CaculateStartingPoint();
    }

    public void AdjustCollider(string colliderPosition,GameObject targetBlock){
        Block tgBlk = targetBlock.GetComponent<Block>();
        Debug.Log("Show total height" + tgBlk.totalHeight);
        foreach(BoxCollider2D obj in tgBlk.colliderList){
            switch(obj.tag){
                case"TopCollider":
                    // obj.offset = new Vector2(boxColliderPosList[obj.tag].x,boxColliderPosList[obj.tag].y+(totalHeight-holeHeight)/2);
                    obj.offset = new Vector2(boxColliderPosList[obj.tag].x,(tgBlk.totalHeight+holeHeight)/2);

                    break;
                case"MidCollider":
                    // obj.offset = new Vector2(boxColliderPosList[obj.tag].x,boxColliderPosList[obj.tag].y+(totalHeight-holeHeight)/2);
                    break;
                case"BotCollider":
                    // obj.offset = new Vector2(boxColliderPosList[obj.tag].x,boxColliderPosList[obj.tag].y-(totalHeight-holeHeight)/2);
                    obj.offset = new Vector2(boxColliderPosList[obj.tag].x,-(tgBlk.totalHeight+holeHeight)/2);

                    break;
                default: break;
            }
        }
    }
    public void CalculateStartingPoint(){
        // startingPointList[0] =new Vector2(60,totalHeight/2);    // Left Top Corner , No hole
        // startingPointList[1] =new Vector2(0,totalHeight/2);      // LEft Top Corner , 1 hole       
        startingPointList[0] =new Vector2(60,this.GetComponent<RectTransform>().sizeDelta.y/2);    // Left Top Corner , No hole
        startingPointList[1] =new Vector2(0,this.GetComponent<RectTransform>().sizeDelta.y/2);   
    }
    public void AdjustPosition(bool startingPointSame){
        // float yPos;
        Debug.Log("Get blkList.Count" + blkList.Count + this.name);
        for(int i = 1; i< blkList.Count;i++){
            blkList[i].transform.localPosition =  new Vector2 (startingPointSame? startingPointList[collidingBlock.holeNumber].x: -startingPointList[holeNumber].x, startingPointList[holeNumber].y - totalHeight - collidingBlock.height/2);
            Debug.Log("Stop");
        }


    }
    public void ReadImage(string blockType, string blockName){
        blockImage.sprite = Resources.Load<Sprite>("BlockImage/"+blockType+" Blocks/" + blockName);
        this.gameObject.GetComponent<RectTransform>().sizeDelta = blockImage.sprite.rect.size;
    }
    public void ParentResetState(){
        if(parentBlock!= null){
        foreach(Block blk in this.blkList){
           parentBlock.RemoveBlock(parentBlock.blkList,blk);
        }
        // parentBlock.GetComponent<RectTransform>().sizeDelta = new Vector2 (parentBlock.width, parentBlock.totalHeight);

            parentBlock.SetColliderActive(true);
            
            AdjustHeight(parentBlock.blkList,parentBlock.gameObject,parentCollider.name);
            
            if(parentCollider.name == "MidCollider")
               innerHoleBool =  holeNumber == 0 ? true : false;
        }
            this.parentBlock = null;
            // this.innerBlock = null;
            this.parentCollider = null;
            this.transform.SetParent(this.canvasManager.transform);

    }
    public void InnerResetState(){
            // AdjustHeight(parentBlock.blkList,parentBlock.gameObject,parentCollider.name);
    }
    public void SetColliderActive(bool b){
        foreach(BoxCollider2D obj in colliderList)
        {
            colliderSwitchList[obj.tag] = b;
        }
    }
    // public void InnerBlockColliderActive(bool b){
    //     SetColliderActive(b);
    //     if(innerBlock !=null)
    //         innerBlock.InnerBlockColliderActive(b);
    // }

    // public void RearrangeBlocks(){
    //     float pPos = 0;
    //     foreach(Block blk in blkList){
    //         blk.transform.localPosition= new Vector2(this.transform.localPosition.x,pPos + blk.hei )
    //         if(blk.innerBlock != null){
    //             RearrangeBlocks();

    //         }
    //     }       
    // }


/////////// 
    public void OnTriggerEnter2D(Collider2D other){
         if(dragBool){
            if(other.name.Substring(0,5) != "Block"){
                collisionBool = true;
                collideItem = other;
                parentCollider = other;

                collidingBlock =other.transform.parent.GetComponent<Block>();

                if(!previewBool && collidingBlock != this && collidingBlock.transform.parent.parent.GetComponent<Block>() != this ){
                    switch(collideItem.tag){
                        case "TopCollider":
                            PreviewBlock(collideItem.name);
                            previewBool = true;
                            break;   
                        case "MidCollider": 
                            PreviewBlock(collideItem.name);
                            previewBool = true;
                            break;
                        case "BotCollider":
                            PreviewBlock(collideItem.name);
                            previewBool = true;   
                            break;
                        case "OutCollider":
                            PreviewBlock(collideItem.name);
                            previewBool = true; 
                            break;
                        default: break;
                    }
                }
            }               
        }
    }


    public void OnTriggerExit2D(Collider2D other){

        if(previewBool && dragBool ){
            Block otherBlk = other.transform.parent.GetComponent<Block>();
            previewBool = false;
            Debug.Log("get " + otherBlk.totalHeight);
            otherBlk.GetComponent<RectTransform>().sizeDelta = new Vector2 (otherBlk.width, otherBlk.innerBlock !=null? otherBlk.totalHeight : otherBlk.height);  // This is the problem 
            // need to find all situations 
            
            collidingBlock = null;
            GameObject.Destroy(this.previewShadow); 
            if(otherBlk.blkList.Count>1){
                bool signedBool = otherBlk.blkList[0].holeNumber == otherBlk.blkList[1].holeNumber;
                int signed = otherBlk.blkList[1].holeNumber- otherBlk.blkList[0].holeNumber;
                otherBlk.blkList[1].transform.localPosition = new Vector2(signedBool?  startingPointList[1].x: signed*startingPointList[0].x, otherBlk.startingPointList[holeNumber].y - otherBlk.blkList[0].height - otherBlk.blkList[1].height/2 +TOP_SPACE );

            }
            // previewBlkList.RemoveAt(previewBlkList.Count-1);
            // previewBlkList.Capacity=previewBlkList.Count-1;
            // if(otherBlk ==null) return;
            // AdjustHeight(previewBlkList,otherBlk.gameObject,collideItem.name);

        }

        collideItem = null;

        collisionBool = false;
    }
    
    public void OnDrag(PointerEventData eventData){
        dragBool = true;

        currentDragBlock = eventData.pointerDrag.GetComponent<Block>();
        if( parentBlock != null){
            ParentResetState();
        }
        if( innerBlock != null){
            InnerResetState();
        }
        SetColliderActive(false);
        Block tempBlk = this.innerBlock;
        while(tempBlk != null){
            tempBlk.SetColliderActive(false);
            tempBlk = tempBlk.innerBlock;
        }

        if(!startDrag){
            startDrag= true;
            touchOffset = Input.mousePosition - transform.position;     
        }
        transform.position = Input.mousePosition - touchOffset;
        this.GetComponent<BoxCollider2D>().enabled = true;
    }
    public void OnEndDrag(PointerEventData eventData){
        // if(otherBlock != null)
        //     otherBlock.ClearList(backupBlkList);
        dragBool = false;
        // SetColliderActive(false);
        if(this.collisionBool){
            InsertBlock(collideItem.name,this.gameObject);         
        }else
        {
            SetColliderActive(true);
        }
        if(innerBlock != null)
            innerBlock.SetColliderActive(true);
        this.GetComponent<BoxCollider2D>().enabled = false;

        startDrag= false;
        previewBool = false;
    }
}