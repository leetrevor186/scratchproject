﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundBlock : Block
{
    // Start is called before the first frame update
    public enum Subtype {PlayUntil,Start,Stop,SetPitch,Clear,ChangeVolume,SetY,EdgeBounce,RotationStyle};

    private BlockType.Catagory catagory = BlockType.Catagory.Sound;
    public Subtype blockSubtype;
    public override void Start()
    {
        base.Start();
       ReadImage(catagory.ToString(),blockSubtype.ToString());
    }

}
