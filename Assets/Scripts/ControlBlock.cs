﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBlock : Block
{
    // Start is called before the first frame update
    public enum Subtype {Wait,Repeat,Forever,IfThen,IfThenElse,WaitUntil,RepeatUntil,Stop,CloneInit,CreateClone,DestroyClone};

    private BlockType.Catagory catagory = BlockType.Catagory.Control;
    public Subtype blockSubtype;
    public override void Start()
    {
        base.Start();
       ReadImage(catagory.ToString(),blockSubtype.ToString());
    }



}
