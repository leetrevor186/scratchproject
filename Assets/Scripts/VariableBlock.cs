﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VariableBlock : Block
{
    // Start is called before the first frame update
    public enum Subtype {Set,Change,Show,Hide,Create};

    private BlockType.Catagory catagory = BlockType.Catagory.Variables;
    public Subtype blockSubtype;
    public override void Start()
    {
        base.Start();
       ReadImage(catagory.ToString(),blockSubtype.ToString());
    }


}
