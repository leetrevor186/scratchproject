﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SensingBlock : Block
{
    // Start is called before the first frame update
    public enum Subtype {Touching,TouchingColor,ColorCollide,Distance,AskAndWait,keyPressed,MouseDown,mousex,mousey,DragMOde,Loudness,Timer,ResetTimer};

    private BlockType.Catagory catagory = BlockType.Catagory.Sensing;
    public Subtype blockSubtype;
    public override void Start()
    {
        base.Start();
       ReadImage(catagory.ToString(),blockSubtype.ToString());
    }


}
