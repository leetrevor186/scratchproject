﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventBlock : Block
{
    // Start is called before the first frame update
    public enum Subtype {WhenClicked,WhenKeyPressed,WhenSpriteClicked,WhenBgSwitched,WhenTimer,WhenReceived,Broadcast,BroadCastAndWait};

    private BlockType.Catagory catagory = BlockType.Catagory.Events;
    public Subtype blockSubtype;
    public override void Start()
    {
        base.Start();
       ReadImage(catagory.ToString(),blockSubtype.ToString());
    }


}
