﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotionBlock : Block
{
    // Start is called before the first frame update
    public enum Subtype {Move,Turn,GoTo,Glide,PointTo,SetX,SetY,EdgeBounce,RotationStyle};
    private BlockType.Catagory catagory = BlockType.Catagory.Motion;
    public Subtype blockSubtype;
    public override void Start()
    {
        base.Start();
       ReadImage(catagory.ToString(),blockSubtype.ToString());
    }



}
